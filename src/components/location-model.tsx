import { ErrorMessage, Formik } from "formik";
import { FC } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import * as yup from "yup";

interface LocationModelProps {
  show: boolean;
  setShow: (arg0: boolean) => void;
  position: google.maps.LatLng|undefined;
}

export const LocationModel: FC<LocationModelProps> = ({
  show,
  setShow,
  position,
}) => {
  return (
    <Modal show={show} onHide={() => setShow(false)}>
      <Modal.Header closeButton>
        <Modal.Title>Pin Location Details</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Formik
          initialValues={{
            type: "",
            name: "",
            lat: position?.toJSON().lat,
            lng: position?.toJSON().lng,
          }}
          validationSchema={yup.object({
            type: yup.string().required("Location Type is required"),
            name: yup.string().required("Location name is required").max(200),
          })}
          onSubmit={(values, { resetForm }) => {
            console.log(values);
            resetForm();
            setShow(false);
            alert("Saved Successfully");
          }}
        >
          {(formik) => (
            <Form onSubmit={formik.handleSubmit}>
              <Form.Group className="my-2">
                <Form.Label>Location Name</Form.Label>
                <Form.Control
                  {...formik.getFieldProps("name")}
                  name="name"
                  placeholder="Please Enter Location Name"
                />
                <ErrorMessage name="name">
                  {(msg) => <span className="text-danger">{msg}</span>}
                </ErrorMessage>
              </Form.Group>
              <Form.Group>
                <Form.Label>Location Type</Form.Label>
                <Form.Select {...formik.getFieldProps("type")} name="type">
                  <option value="" disabled>
                    Please Select
                  </option>
                  <option>Cafe</option>
                  <option>Hotel</option>
                  <option>POI</option>
                </Form.Select>
                <ErrorMessage name="type">
                  {(msg) => <span className="text-danger">{msg}</span>}
                </ErrorMessage>
              </Form.Group>
              <Form.Group className="mt-5">
                <Button
                  type="button"
                  variant="secondary"
                  onClick={() => setShow(false)}
                  className="mr-3"
                >
                  Close
                </Button>
                <Button className="mx-2" type="submit" variant="primary">
                  Save
                </Button>
              </Form.Group>
            </Form>
          )}
        </Formik>
      </Modal.Body>
    </Modal>
  );
};

export default LocationModel;
