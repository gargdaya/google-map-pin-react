import { Wrapper, Status } from "@googlemaps/react-wrapper";
import { Map } from "./components/map";
import { FC, useEffect, useState } from "react";
import { Marker } from "./components/marker";
// import "bootstrap/dist/css/bootstrap.min.css";
import LocationModel from "./components/location-model";

const render = (status: Status) => {
  return <h1>{status}</h1>;
};

const App: FC = () => {
  const [position, setPosition] = useState<google.maps.LatLng>();
  const [zoom] = useState(7); // initial zoom
  const [center, setCenter] = useState<google.maps.LatLngLiteral>({
    lat: 0,
    lng: 0,
  });
  const [show, setShow] = useState(false);
  const onClick = (e: google.maps.MapMouseEvent) => {
    setPosition(e.latLng!);
    setShow(true);
  };
  useEffect(() => {
    navigator.geolocation.getCurrentPosition((data) => {
      setPosition(
        new google.maps.LatLng({
          lat: data.coords.latitude,
          lng: data.coords.longitude,
        })
      );
      setCenter({
        lat: data.coords.latitude,
        lng: data.coords.longitude,
      });
    });
  }, []);

  return (
    <div style={{ display: "flex", height: "100vh" }}>
      <Wrapper apiKey={process.env.REACT_APP_API_KEY || ""} render={render}>
        <Map
          center={center}
          onClick={onClick}
          
          zoom={zoom}
          style={{ flexGrow: "1", height: "100%" }}
        >
          <Marker position={position} />
        </Map>
      </Wrapper>
      <LocationModel show={show} setShow={setShow} position={position} />
    </div>
  );
};

export { App };
